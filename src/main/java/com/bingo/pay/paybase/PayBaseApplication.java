package com.bingo.pay.paybase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class PayBaseApplication {


  public static void main(String[] args) {
    SpringApplication.run(PayBaseApplication.class, args);
  }

}
